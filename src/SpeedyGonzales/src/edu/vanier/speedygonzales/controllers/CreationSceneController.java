package edu.vanier.speedygonzales.controllers;

import edu.vanier.speedygonzales.elements.Circle;
import edu.vanier.speedygonzales.helpers.Attributes;
import edu.vanier.speedygonzales.elements.Diamond;
import edu.vanier.speedygonzales.elements.Element;
import edu.vanier.speedygonzales.elements.Equal;
import edu.vanier.speedygonzales.elements.Hexagon;
import edu.vanier.speedygonzales.elements.Square;
import edu.vanier.speedygonzales.elements.Squircle;
import edu.vanier.speedygonzales.elements.Triangle;
import edu.vanier.speedygonzales.helpers.UIMethods;
import edu.vanier.speedygonzales.helpers.UpdateDatabase;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 * FXML Controller class for creationScene
 *
 * @author Speedy Gonzales
 */
public class CreationSceneController implements Initializable {

    private GraphicsContext gc;
    private double windowSize;

    @FXML private ImageView imgBack;
    @FXML private Canvas canvas;
    @FXML private StackPane stackDesign;
    @FXML private SplitPane splitPane;

    @FXML private ToggleGroup axisGroup;
    @FXML private ToggleGroup modeGroup;
    @FXML private ToggleGroup backgroundGroup;

    @FXML private RadioButton rdbXY;
    @FXML private RadioButton rdbX;
    @FXML private RadioButton rdbY;

    @FXML private MenuItem menuSave;
    @FXML private MenuItem menuExport;
    @FXML private MenuItem menuOpen;
    @FXML private MenuItem menuClose;
    @FXML private MenuItem menuRefresh;

    @FXML private ToggleButton tgbNormal;
    @FXML private ToggleButton tgbXRay;
    @FXML private ToggleButton tgbDashed;
    @FXML private ToggleButton tgbQuad;
    @FXML private ToggleButton tgbImgEnabled;
    @FXML private ToggleButton tgbImgDisabled;

    private Button btnExport;
    @FXML private Button btnStopAnim;
    @FXML private Button btnRandom;
    @FXML private Button btnAnimate;
    @FXML private Button btnSplitPane;

    @FXML private Slider sliderRotation;
    @FXML private Slider sliderDepth;
    @FXML private Slider sliderSpacing;
    @FXML private Slider sliderStrokeSize;
    @FXML private Slider sliderDashedSize;
    @FXML private Slider sliderInitialSize;
    @FXML private Slider sliderXAnchor;
    @FXML private Slider sliderYAnchor;
    @FXML private Slider sliderXPosition;
    @FXML private Slider sliderYPosition;

    @FXML private ComboBox cmbElement;
    @FXML private ComboBox cmbBlendMode;
    @FXML private ComboBox cmbBackImage;

    @FXML private ColorPicker cpFirstColorPicker;
    @FXML private ColorPicker cpSecondColorPicker;

    @FXML private TextField txtSaveName;

    @FXML private TabPane tabScenes;
    @FXML private AnchorPane apLibrary;
    @FXML private Menu menuFile;
    @FXML private Tab tabDesign;
    @FXML private AnchorPane paneRoot;
    @FXML private TilePane tilePane;
    @FXML private AnchorPane apCanvas;
    @FXML private VBox vboxStrokeSize;
    @FXML private VBox vboxStrokeSize1;
    @FXML private CheckMenuItem cmiType;
    @FXML private CheckMenuItem cmiMode;
    @FXML private CheckMenuItem cmiDepth;
    @FXML private CheckMenuItem cmiSpacingRatio;
    @FXML private CheckMenuItem cmiRotation;
    @FXML private CheckMenuItem cmiAxis;
    @FXML private CheckMenuItem cmiAnchor;
    @FXML  private CheckMenuItem cmiFirstColor;
    @FXML private CheckMenuItem cmiSecondColor;
    @FXML private CheckMenuItem cmiXRayLine;

    private CheckMenuItem cmiBackAndForth;
    @FXML private CheckMenuItem cmiInfZoom;
    @FXML private CheckMenuItem cmiRainbow;
    @FXML private MenuButton menuBtnAnimations;
    @FXML private ScrollPane spLibrary;
    @FXML private BorderPane bpCanvas;
    @FXML private Label lblInfo;
    @FXML private Tab tabLibrary;
    @FXML
    private CheckMenuItem cmiInitialSize;
    @FXML
    private CheckMenuItem cmiBlendList;
    @FXML
    private CheckMenuItem cmiBlendImage;
    @FXML
    private CheckMenuItem cmiBackgroundSelection;
    @FXML
    private CheckMenuItem cmiBouncing;
    @FXML
    private CheckMenuItem cmiSpaceshift;
    @FXML
    private CheckMenuItem cmiCircular;

    /**
     * events from the menuBar
     *
     * @param event when an item on the menu bar is clicked
     * @throws IOException
     */
    @FXML
    private void menuEvents(ActionEvent event) throws IOException {
        if (menuExport == event.getSource() || btnExport == event.getSource()) {
            UIMethods.exportDesign(stackDesign, canvas);
            //exportDesign();
        }

        if (menuOpen == event.getSource()) {
            tabScenes.getSelectionModel().select(1);

        }

        if (menuSave == event.getSource()) {
            if (getTxtName().isEmpty()) {
                UIMethods.infoBox("Your design has no name saved to it. Please enter a name.", "Alert");
                return;
            }

            for (int i = 0; i < edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().size(); i++) {
                if (getTxtName().equals(edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().get(i).getName())) {
                    confirmBox("Your design has already saved with that name. Do You want to replace it?", "Confirm");
                    return;
                }
            }
            doSaveAttributes();
            apLibrary.getChildren().clear();
            doUpdateLibrary();
        }

        if (menuClose == event.getSource()) {
            Platform.exit();
            System.exit(0);
        }
        if (menuRefresh == event.getSource()) {

            for (int i = 0; i < edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().size(); i++) {
                if (getTxtName().equals(edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().get(i).getName())) {
                    openDesign(getTxtName());
                    return;
                }
            }
            defaultDrawing();
        }
    }

    private void handleSaveEvent(ActionEvent event) {
        if (getTxtName().isEmpty()) {
            UIMethods.infoBox("Your design has no name saved to it. Please enter a name.", "Alert");
            return;
        }

        for (int i = 0; i < edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().size(); i++) {
            if (getTxtName().equals(edu.vanier.speedygonzales.helpers.UpdateDatabase.getAttributeName().get(i).getName())) {
                confirmBox("Your design has already saved with that name. Do You want to replace it?", "Confirm");
                return;
            }
        }
        doSaveAttributes();
        apLibrary.getChildren().clear();
        doUpdateLibrary();

    }

    @FXML
    private void splitPaneEvent(ActionEvent event) {
        if (splitPane.getDividers().get(0).getPosition() <= 0.98) {
            splitPane.setDividerPositions(1);
            btnSplitPane.setRotate(0);
        } else if (splitPane.getDividers().get(0).getPosition() >= 0.98) {
            splitPane.setDividerPositions(0.66);
            btnSplitPane.setRotate(180);
        }
    }

    @FXML
    private void generateRandomEvent(ActionEvent event) {
        doGenerateRandom();
    }

    @FXML
    private void mouseEvent(MouseEvent event) {
        double coordinateX = event.getX();
        double coordinateY = event.getY();

        if (event.isShortcutDown() == false) {
            sliderXAnchor.setValue(coordinateX);
            sliderYAnchor.setValue(coordinateY);
        } else if (event.isShortcutDown() == true) {
            sliderXPosition.setValue(coordinateX);
            sliderYPosition.setValue(coordinateY);
        }
    }

    @FXML
    private void handleBtnAnimateEvent(MouseEvent event) {
        //liveAttributeChange();
    }

    /**
     * handles events related to library
     *
     * @param event
     */
    private void handleLibraryEvent(Event event) {
        doUpdateLibrary();
    }

    /**
     * open a design from the library
     *
     * @param name the name of the design
     */
    private void openDesign(String name) {
        txtSaveName.setText(name);

        Attributes attribute = UpdateDatabase.getAttribute(name);

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getHeight(), canvas.getHeight());

        sliderDepth.setValue(attribute.getDepth());
        sliderRotation.setValue(attribute.getRotationValue());
        sliderSpacing.setValue(attribute.getSpacingRatio());
        sliderStrokeSize.setValue(attribute.getStrokeSize());
        cmbElement.setValue(attribute.getType());
        cpFirstColorPicker.setValue(Color.valueOf(attribute.getFirstColor()));
        cpSecondColorPicker.setValue(Color.valueOf(attribute.getSecondColor()));
        setDataForGroup(attribute.getAxis(), axisGroup);
        sliderXAnchor.setValue(attribute.getAnchorX());
        sliderYAnchor.setValue(attribute.getAnchorY());
        sliderXPosition.setValue(attribute.getPosX());
        sliderYPosition.setValue(attribute.getPosY());
        setDataForGroup(attribute.getMode(), modeGroup);
        if (attribute.getDashed().equals("Selected")) {
            tgbDashed.setSelected(true);
        } else {
            tgbDashed.setSelected(false);
        }
        if (attribute.getQuad().equals("Selected")) {
            tgbQuad.setSelected(true);
        } else {
            tgbQuad.setSelected(false);
        }
        cmbBlendMode.setValue(attribute.getBlendMode());
        setDataForGroup(attribute.getBackgroundSelection(), backgroundGroup);
        cmbBackImage.setValue(attribute.getBackgroundImage());
        sliderInitialSize.setValue(attribute.getInitialSize());
        
        backgroundImageEvent();
        doDrawing();
    }
    
    /**
     * Timeline that calls drawElement.
     */
    public void liveDraw() {

        Timeline liveTimeline = new Timeline(new KeyFrame(Duration.millis(50), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                doDrawing();
                if (windowSize != spLibrary.getBoundsInLocal().getMaxX()) {
                    windowSize = spLibrary.getBoundsInLocal().getMaxX();
                    apLibrary.getChildren().clear();
                    doUpdateLibrary();
                }
                /*if(getCanvasHeight() != canvasSizeUpdate()){
                    canvas.setHeight(canvasSizeUpdate());
                    canvas.setWidth(canvasSizeUpdate()); 
                    gc.setFill(Color.WHITE);
                    gc.fillRect(0, 0, canvas.getHeight(), canvas.getHeight());
                    doDrawing();
                }*/
                    
            }
        }));
        liveTimeline.setCycleCount(Timeline.INDEFINITE);
        liveTimeline.play();
    }

    /**
     * Timeline that changes attributes
     */
    public void liveAttributeChange() {

        Timeline liveTimeline = new Timeline(new KeyFrame(Duration.millis(35), new EventHandler<ActionEvent>() {
        
            Boolean isChanging = true;
            Boolean isIncreasing = true;
            double valueXAnchor = getXAnchor();
            double valueYAnchor = getYAnchor();
            double valueXPosition = getXPos();
            double valueYPosition = getYPos();
            double valueRotation = getRotationValue();
            double valueIntialSize = getInitialSize();
            double valueRatio = getSpacingRatioValue();
            double hue = cpFirstColorPicker.getValue().getHue();
            double saturation = cpFirstColorPicker.getValue().getSaturation();
            double brightness = cpFirstColorPicker.getValue().getBrightness();
            final double radius = 206;
            double origin = getCanvasHeight()/2.00;
            double angle = 0;
            
            
            @Override
            public void handle(ActionEvent event) {

                doDrawing();

                if (cmiBouncing.isSelected()) {
                    
                    sliderRotation.setValue(valueRotation);
                    if (valueRotation == 15) {
                        isIncreasing = false;
                    } else if (valueRotation == -15) {
                        isIncreasing = true;
                    }

                    if (isIncreasing.equals(true)) {
                        valueRotation += 1;
                    } else if (isIncreasing.equals(false)) {
                        valueRotation -= 1;
                    }
                }
                if(cmiSpaceshift.isSelected()){
                    sliderSpacing.setValue(valueRatio);
                    if(valueRatio >= 0.9){
                        isChanging = false;
                    } else if (valueRatio <= 0.7) {
                        isChanging = true;
                    }
                    
                    if (isChanging.equals(true)) {
                        valueRatio += 0.006;
                    } else if (isChanging.equals(false)) {
                        valueRatio -= 0.006;
                    }
                }
                
                if (cmiAnchor.isSelected()) {
                    sliderXAnchor.setValue(valueXAnchor);
                    sliderYAnchor.setValue(valueYAnchor);
                    angle -= 0.038;
                    valueXAnchor = origin + Math.cos(angle) * radius;
                    valueYAnchor = origin + Math.sin(angle) * radius;
                   
                }
                if (cmiCircular.isSelected()) {
                    sliderXPosition.setValue(valueXPosition);
                    sliderYPosition.setValue(valueYPosition);
                    angle += 0.038;
                    valueXPosition = origin + Math.cos(angle) * radius;
                    valueYPosition = origin + Math.sin(angle) * radius;

                }

 

                if (cmiInfZoom.isSelected()) {
                    double ratio = Math.pow(getSpacingRatioValue(), 2);
                    sliderInitialSize.setValue(valueIntialSize);

                    if (valueIntialSize >= (512 / ratio)) {
                        valueIntialSize = 512;
                    } else {
                        valueIntialSize += (((512 / ratio) - 512) / 15);
                    }
                }
                // ----- Code for the rotation liveAttributeChange

                if (cmiRainbow.isSelected()) {

                    cpFirstColorPicker.setValue(Color.hsb(hue, saturation, brightness));
                    if (saturation <= 1) {
                        saturation += 0.1;
                    }
                    if (brightness <= 1) {
                        brightness += 0.1;
                    }

                    if (saturation >= 1) {
                        saturation = 1;
                    }
                    if (brightness >= 1) {
                        brightness = 1;
                    }

                    if (saturation == 1 && brightness == 1) {
                        if (hue == 360) {
                            isChanging = false;
                        } else {
                            isChanging = true;
                        }

                        if (isChanging.equals(true)) {
                            hue += 1.2;
                        } else if (isChanging.equals(false)) {
                            hue = 0;
                        }
                    }
                }

            }

        }));

        liveTimeline.setCycleCount(Timeline.INDEFINITE);
        liveTimeline.pause();
        btnStopAnim.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                liveTimeline.pause();
                btnAnimate.setDisable(false);
                btnStopAnim.setDisable(true);
            }
        });
        btnAnimate.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                liveTimeline.play();
                btnStopAnim.setDisable(false);
                btnAnimate.setDisable(true);
            }
        });
        tabLibrary.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event event) {
                liveTimeline.pause();
                btnAnimate.setDisable(false);
                btnStopAnim.setDisable(true);
            }
        });
        
    }
    
    /**
     * the drawing method
     */
    public void doDrawing() {
        resetBackground();

        imgBack.setFitHeight(getCanvasWidth());
        imgBack.setFitWidth(getCanvasHeight());

        double strokeSize = getStrokeSizeValue(); // Would not be necessary
        Element element = new Element(getInitialSize(), getXAnchor(), getYAnchor(), getXPos(), getYPos(), getCanvasHeight());

        //Generate elements in the canva
        for (int i = 0; i < getDepthValue(); i++) {
            gc.save();
            gc.translate(element.getCoordinateX(), element.getCoordinateY());

            gc.scale(element.getLength() / canvas.getHeight(), element.getHeight() / canvas.getHeight());

            doRotate(i, getRotationValue());

            if (getElementMode().equals("XRay")) {
                strokeSize = strokeSize * getSpacingRatioValue();
                if (tgbQuad.isSelected() == true) {
                    drawTetraStroke();
                } else {
                    drawStroke();
                }
                cpSecondColorPicker.setDisable(true);
                tgbXRay.setDisable(true);
                tgbNormal.setDisable(false);
                tgbDashed.setDisable(false);
                sliderStrokeSize.setDisable(false);
            } else {
                gc.setFill(i % 2 == 0 ? getElementFirstColor() : getElementSecondColor());
                //drawElements();
                if (tgbQuad.isSelected() == true) {
                    drawTetra();
                } else {
                    drawElements();
                }
                cpSecondColorPicker.setDisable(false);
                tgbNormal.setDisable(true);
                tgbXRay.setDisable(false);
                tgbDashed.setDisable(true);
                sliderStrokeSize.setDisable(true);
                sliderDashedSize.setDisable(true);
            }

            if (tgbDashed.isSelected() == true) {
                sliderDashedSize.setDisable(false);
            } else {
                sliderDashedSize.setDisable(true);
            }

            // Update values
            drawUpdate(element);

            gc.restore();
        }
        backgroundImageEvent();
    }

    /**
     * generate random attributes and call doDrawing()
     */
    private void doGenerateRandom() {
        List<String> elemntList = new ArrayList();
        elemntList.add("Square");
        elemntList.add("Circle");
        elemntList.add("Triangle");
        elemntList.add("Squircle");
        elemntList.add("Diamond");
        elemntList.add("Hexagon");

        List<String> axisList = new ArrayList();
        axisList.add("XY");
        axisList.add("X");
        axisList.add("Y");
        
        List<String> blendList = new ArrayList();
        blendList.add("SRC_OVER");
        blendList.add("SRC_ATOP");
        blendList.add("ADD");
        blendList.add("BLUE");
        blendList.add("COLOR_BURN");
        blendList.add("COLOR_DODGE");
        blendList.add("DARKEN");
        blendList.add("DIFFERENCE");
        blendList.add("EXCLUSION");
        blendList.add("GREEN");
        blendList.add("HARD_LIGHT");
        blendList.add("LIGHTEN");
        blendList.add("MULTIPLY");
        blendList.add("OVERLAY");
        blendList.add("RED");
        blendList.add("SCREEN");
        blendList.add("SOFT_LIGHT");
        
        List<String> imageList = new ArrayList();
        imageList.add("Filled");
        imageList.add("Circle");
        imageList.add("Heart");
        imageList.add("Sunray");
        imageList.add("Bladecut");
        imageList.add("Sandglass");
        imageList.add("User");
        imageList.add("Donut");
        imageList.add("Spark");
        imageList.add("Top toy");
        imageList.add("Bee");
        imageList.add("Crossroad");
        imageList.add("Honeycomb");
        imageList.add("Lucky Clover");
        
        Random rand = new Random();

        List<String> modeList = new ArrayList();
        modeList.add("Normal");
        modeList.add("XRay");
        
        List<String> bdSelection = new ArrayList();
        bdSelection.add("Disable");
        bdSelection.add("Enable");

        if (cmiDepth.isSelected()) {
            sliderDepth.setValue((int) getRandomDoubleBetweenRange(sliderDepth.getMin(), sliderDepth.getMax()));
        }
        if (cmiRotation.isSelected()) {
            sliderRotation.setValue(getRandomDoubleBetweenRange(sliderRotation.getMin(), sliderRotation.getMax()));
        }
        if (cmiSpacingRatio.isSelected()) {
            sliderSpacing.setValue(getRandomDoubleBetweenRange(sliderSpacing.getMin(), sliderSpacing.getMax()));
        }
        if (cmiXRayLine.isSelected()) {
            sliderStrokeSize.setValue(getRandomDoubleBetweenRange(sliderStrokeSize.getMin(), sliderStrokeSize.getMax()));
        }
        if (cmiType.isSelected()) {
            cmbElement.setValue(elemntList.get(getRandomIntBetweenRange(0, elemntList.size() - 1)));
        }
        if (cmiAxis.isSelected()) {
            setDataForGroup(axisList.get(getRandomIntBetweenRange(0, axisList.size() - 1)), axisGroup);
        }
        if (cmiMode.isSelected()) {
            setDataForGroup(modeList.get(getRandomIntBetweenRange(0, modeList.size() - 1)), modeGroup);
        }
        if (cmiFirstColor.isSelected()) {
            cpFirstColorPicker.setValue(Color.valueOf(String.format("#%06x", rand.nextInt(0xffffff + 1))));
        }
        if (cmiSecondColor.isSelected()) {
            cpSecondColorPicker.setValue(Color.valueOf(String.format("#%06x", rand.nextInt(0xffffff + 1))));
        }
        if (cmiAnchor.isSelected()) {
            sliderXAnchor.setValue(getRandomDoubleBetweenRange(sliderXAnchor.getMin(), sliderXAnchor.getMax()));
            sliderYAnchor.setValue(getRandomDoubleBetweenRange(sliderYAnchor.getMin(), sliderYAnchor.getMax()));
        }
        if(cmiInitialSize.isSelected()){
            sliderInitialSize.setValue(getRandomIntBetweenRange((int)(sliderInitialSize.getMin()), (int)(sliderInitialSize.getMax())));
        }
        if(cmiBlendImage.isSelected()){
            cmbBackImage.setValue(imageList.get(getRandomIntBetweenRange(0, imageList.size() - 1)));
        }
        if(cmiBlendImage.isSelected()){
            cmbBlendMode.setValue(blendList.get(getRandomIntBetweenRange(0, blendList.size() - 1)));
        }
        if(cmiBackgroundSelection.isSelected()){
            setDataForGroup(bdSelection.get(getRandomIntBetweenRange(0, bdSelection.size() - 1)), backgroundGroup);
        }
        doDrawing();
    }
    
    /**
     * Save the attributes in library / database
     */
    private void doSaveAttributes() {
        
        if (getTxtName().length() >= 20) {
            UIMethods.infoBox("The name of your design should be smaller than 20 characters.", "Warning");
            return;
        }
        else {
            UpdateDatabase.saveAttributes(getTxtName(), getElementType(), getDepthValue(),
                getSpacingRatioValue(), getRotationValue(), getAxisValue(),
                getElementFirstColor(), getElementSecondColor(), UIMethods.imagesInBytes(stackDesign,canvas),
                getStrokeSizeValue(), getElementMode(), tgbDashSelection(),
                tgbQuadSelection(), getElementBlendMode(), backgroundImgSelected(),
                getBackgroundImg(), getXAnchor(), getYAnchor(), getXPos(), getYPos(), getInitialSize());
            
            String message = getTxtName() + " sucessfully saved!";
            UIMethods.showUpdate(message, lblInfo);
        }

    }

    private void doUpdateAttributes(String txtName) {
        UpdateDatabase.updateAttributes(txtName, getElementType(), getDepthValue(),
                getSpacingRatioValue(), getRotationValue(), getAxisValue(),
                getElementFirstColor(), getElementSecondColor(), UIMethods.imagesInBytes(stackDesign,canvas),
                getStrokeSizeValue(), getElementMode(), tgbDashSelection(),
                tgbQuadSelection(), getElementBlendMode(), backgroundImgSelected(),
                getBackgroundImg(), getXAnchor(), getYAnchor(), getXPos(), getYPos(), getInitialSize());
    }

    /**
     * method to update the library scene
     */
    public void doUpdateLibrary() {
        int x = 10;
        int y = 25;

        for (int i = 0; i < UpdateDatabase.getAttributeName().size(); i++) {

            ImageView image = UpdateDatabase.getAttributeName().get(i).getImage();
            String name = UpdateDatabase.getAttributeName().get(i).getName();

            VBox vb = new VBox();
            vb.setLayoutX(x);
            vb.setLayoutY(y);
            vb.setSpacing(5);
            vb.setAlignment(Pos.CENTER);

            HBox hb = new HBox();

            hb.setSpacing(10);
            hb.setAlignment(Pos.CENTER_LEFT);

            vb.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;"
                    + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
                    + "-fx-border-radius: 5;" + "-fx-border-color: black;");

            Label lblName = new Label(name);

            Image imgDelete = new Image("edu/vanier/speedygonzales/images/delete.png");
            Button delete = new Button();
            delete.setGraphic(new ImageView(imgDelete));
            delete.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    UpdateDatabase.delete(name);
                    System.out.println(name + " is deleted!");
                    apLibrary.getChildren().clear();
                    doUpdateLibrary();
                }
            });

            Image imgOpen = new Image("edu/vanier/speedygonzales/images/open.png");
            Button open = new Button();
            open.setGraphic(new ImageView(imgOpen));
            open.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println("Opened!");

                    openDesign(name);
                    tabScenes.getSelectionModel().selectPrevious();
                }
            });

            Image imgExport = new Image("edu/vanier/speedygonzales/images/export.png");
            Button export = new Button();
            export.setGraphic(new ImageView(imgExport));
            export.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    System.out.println("Export!");
                    openDesign(name);
                    UIMethods.exportDesign(stackDesign, canvas);
                }
            });

            image.setSmooth(true);

            Separator separator = new Separator();
            separator.setStyle("-fx-background-color: black;" + "-fx-background-radius: 2");

            hb.getChildren().addAll(open, delete, export);
            vb.getChildren().addAll(image, separator, lblName, hb);

            apLibrary.getChildren().add(vb);

            x += 250;
            if (x+200 > windowSize) {
                x = 10;
                y += 300;
            }
        }

        VBox vbNew = new VBox();

        vbNew.setLayoutX(x);
        vbNew.setLayoutY(y);
        vbNew.setSpacing(35);
        vbNew.setAlignment(Pos.CENTER);
        vbNew.setStyle("-fx-padding: 10;" + "-fx-border-style: solid inside;"
                + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
                + "-fx-border-radius: 5;" + "-fx-border-color: black;");

        ImageView imgTest = new ImageView(new Image("edu/vanier/speedygonzales/images/add.png"));
        vbNew.setMargin(imgTest, new Insets(50, 6, 50, 6));

        vbNew.getChildren().addAll(imgTest);
        vbNew.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resetBackground();
                defaultDrawing();

                doDrawing();
                tabScenes.getSelectionModel().selectPrevious();
            }
        });
        apLibrary.getChildren().add(vbNew);

        x += 250;
        if (x+200 >= windowSize) {
            x = 10;
            y += 300;
        }
    }

    /**
     * This method generates filled elements
     */
    public void drawElements() {
        if (getElementType().equals("Square")) 
            Square.drawElement(gc);            
        
        if (getElementType().equals("Circle")) 
            Circle.drawElement(gc);
        
        if (getElementType().equals("Triangle")) 
            Triangle.drawElement(gc);

        if (getElementType().equals("Diamond")) 
            Diamond.drawElement(gc);

        if (getElementType().equals("Squircle")) 
            Squircle.drawElement(gc);
        
        if (getElementType().equals("Hexagon")) 
            Hexagon.drawElement(gc);
        
        if (getElementType().equals("Equal")) 
            Equal.drawElement(gc);
        
    }

    /**
     * This method generates stoke elements
     *
     */
    public void drawStroke() {

        gc.setStroke(cpFirstColorPicker.getValue());
        gc.setLineWidth(sliderStrokeSize.getValue());

        if (tgbDashed.isSelected() == true) {
            gc.setLineDashes(sliderDashedSize.getValue());
        }

        if (getElementType().equals("Square")) 
            Square.drawStroke(gc);
        
        if (getElementType().equals("Circle")) 
            Circle.drawStroke(gc);
        
        if (getElementType().equals("Triangle")) 
            Triangle.drawStroke(gc);

        if (getElementType().equals("Diamond")) 
            Diamond.drawStroke(gc);

        if (getElementType().equals("Hexagon")) 
            Hexagon.drawStroke(gc);

        if (getElementType().equals("Squircle")) 
            Squircle.drawStroke(gc);

        if (getElementType().equals("Equal")) 
            Equal.drawStroke(gc);

    }

    /**
     * draw 4 elements in the canvas with normal mode
     */
    public void drawTetra() {

        if (getElementType().equals("Square")) 
            Square.drawTetra(gc);
            
         else if (getElementType().equals("Circle")) 
            Circle.drawTetra(gc);
            
         else if (getElementType().equals("Triangle")) 
             Triangle.drawTetra(gc);
         
         else if (getElementType().equals("Diamond")) 
            Diamond.drawTetra(gc);
        
         else if (getElementType().equals("Squircle")) 
            Squircle.drawTetra(gc);
         
         else if (getElementType().equals("Hexagon")) 
            Hexagon.drawTetra(gc);
            
         else if (getElementType().equals("Equal")) 
            Equal.drawTetra(gc);
        
    }

    /**
     * draw 4 elements in the canvas with XRay(Stroke)
     */
    public void drawTetraStroke() {

        gc.setStroke(cpFirstColorPicker.getValue());
        gc.setLineWidth(sliderStrokeSize.getValue());

        if (tgbDashed.isSelected() == true) 
            gc.setLineDashes(sliderDashedSize.getValue());
        

        if (getElementType().equals("Square")) 
            Square.drawTetraStroke(gc);
        
         else if (getElementType().equals("Circle")) 
            Circle.drawTetraStroke(gc);
         
         else if (getElementType().equals("Triangle")) 
            Triangle.drawTetraStroke(gc);
         
         else if (getElementType().equals("Diamond")) 
            Diamond.drawTetraStroke(gc);
            
         else if (getElementType().equals("Squircle")) 
             Squircle.drawTetraStroke(gc);
         
         else if (getElementType().equals("Hexagon")) 
            Hexagon.drawTetraStroke(gc);
        
         else if (getElementType().equals("Equal")) 
            Equal.drawTetraStroke(gc);
        
    }

    /**
     * Update the coordinates values
     *
     * @param element an instance of element class
     */
    private void drawUpdate(Element element) {
        double spacingRatio = getSpacingRatioValue();

        if (getAxisValue().equals("XY")) {
            element.coordinateaUpdatorXY(spacingRatio, getXAnchor(), getYAnchor());
        } else if (getAxisValue().equals("X")) {
            element.coordinateaUpdatorX(spacingRatio, getXAnchor());
        } else if (getAxisValue().equals("Y")) {
            element.coordinateaUpdatorY(spacingRatio, getYAnchor());
        }
    }

    /**
     * reset the graphicalContext background to white
     */
    public void resetBackground() {
        //Repition?
        if (getElementMode().equals("XRay")) {
            gc.setFill(Color.WHITE);
        } else {
            gc.setFill(Color.WHITE);
        }

        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * event for the background design image
     *
     * @param event
     */
    private void backgroundImageEvent() {
        if (backgroundImgSelected().equals("Disabled")) {
            imgBack.setVisible(false);
            tgbImgDisabled.setDisable(true);
            tgbImgEnabled.setDisable(false);
            canvas.setBlendMode(BlendMode.valueOf("SRC_OVER"));
        }
        if (backgroundImgSelected().equals("Enabled")) {
            imgBack.setVisible(true);
            tgbImgDisabled.setDisable(false);
            tgbImgEnabled.setDisable(true);
            canvas.setBlendMode(BlendMode.valueOf(cmbBlendMode.getValue().toString()));
        }
        imgBack.setImage(UIMethods.getImg(cmbBackImage));
    }

    /**
     * get the spacing ratio attribute
     *
     * @return the spacingRatio valueRotation
     */
    private double getSpacingRatioValue() {
        return sliderSpacing.getValue();
    }

    /**
     * get the drawStroke size attribute
     *
     * @return the s valueRotation
     */
    private double getStrokeSizeValue() {
        return sliderStrokeSize.getValue();
    }

    /**
     * get the depth attribute
     *
     * @return the depth valueRotation
     */
    private int getDepthValue() {
        return (int) sliderDepth.getValue();
    }

    /**
     * get the rotation attribute
     *
     * @return the doRotate valueRotation
     */
    private double getRotationValue() {
        return sliderRotation.getValue();
    }

    /**
     * get the size of the first element to generate
     *
     * @return a double value
     */
    private double getInitialSize() {
        return sliderInitialSize.getValue();
    }

    /**
     * method to doRotate shape
     *
     * @param i increment
     * @param rotateValue the degree of rotation
     */
    public void doRotate(int i, double rotateValue) {
        gc.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);
        gc.rotate(i * rotateValue);
        gc.translate(-canvas.getWidth() / 2, -canvas.getHeight() / 2);
    }

    /**
     * get the type attribute
     *
     * @return the type of element
     */
    private String getElementType() {
        return (String) cmbElement.getValue();
    }

    /**
     * get first color of the design
     *
     * @return color of the type
     */
    private Color getElementFirstColor() {
        return (Color) cpFirstColorPicker.getValue();
    }

    /**
     * get second color of the design
     *
     * @return color of the type
     */
    private Color getElementSecondColor() {
        return (Color) cpSecondColorPicker.getValue();
    }

    /**
     * get the canvas name entered in the textBox
     *
     * @return the name
     */
    public String getTxtName() {
        return txtSaveName.getText();
    }

    /**
     * get the generation mode
     *
     * @return a string representation of the mode
     */
    public String getElementMode() {
        return modeGroup.getSelectedToggle().getUserData().toString();
    }

    /**
     * get the blend mode
     *
     * @return a sting representation of the mode
     */
    public String getElementBlendMode() {
        return cmbBlendMode.getValue().toString();
    }

    /**
     * to get the sliderXAnchor value
     *
     * @return the double value of sliderXAnchor
     */
    public double getXAnchor() {
        return sliderXAnchor.getValue();
    }

    /**
     * to get the sliderYAnchor value
     *
     * @return the double value of sliderYAnchor
     */
    public double getYAnchor() {
        return sliderYAnchor.getValue();
    }

    public double getXPos() {
        return sliderXPosition.getValue();
    }

    public double getYPos() {
        return sliderYPosition.getValue();
    }

    /**
     * check if background image is selected
     *
     * @return
     */
    public String backgroundImgSelected() {
        return backgroundGroup.getSelectedToggle().getUserData().toString();
    }

    /**
     * get the canvas width
     *
     * @return a double value of the width
     */
    public double getCanvasWidth() {
        return canvas.getWidth();
    }

    /**
     * get the canvas height
     *
     * @return a double value of the height
     */
    public double getCanvasHeight() {
        return canvas.getHeight();
    }

    /**
     * get the axis selected
     *
     * @return a string of the axis name
     */
    public String getAxisValue() {
        return axisGroup.getSelectedToggle().getUserData().toString();
    }

    /**
     * get the background image
     *
     * @return the string name of the background image
     */
    public String getBackgroundImg() {
        return cmbBackImage.getValue().toString();
    }

    /**
     * method to return a random double from specified range
     *
     * @param min is the minimum of range
     * @param max is the maximum of range
     * @return
     */
    public double getRandomDoubleBetweenRange(double min, double max) {
        double x = (Math.random() * ((max - min) + 1)) + min;
        return x;
    }

    /**
     * method to return a random int from specified range
     *
     * @param min is the minimum of range
     * @param max is the maximum of range
     * @return
     */
    public int getRandomIntBetweenRange(int min, int max) {
        int x = (int) ((Math.random() * ((max - min) + 1)) + min);
        return x;
    }

    public boolean tgbDashSelection() {
        return tgbDashed.isSelected();
    }

    public boolean tgbQuadSelection() {
        return tgbQuad.isSelected();
    }

    /**
     * set data of a toggleGroup
     *
     * @param data the datas
     * @param group the toggle group
     */
    public void setDataForGroup(String data, ToggleGroup group) {
        for (int i = 0; i < group.getToggles().size(); i++) {
            if (group.getToggles().get(i).getUserData().toString().equals(data)) {
                group.selectToggle(group.getToggles().get(i));
            }
        }
    }

    /**
     * method to display confirmation pop-up message
     *
     * @param infoMessage
     * @param titleBar
     */
    public void confirmBox(String infoMessage, String titleBar) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(titleBar);
        //alert.setHeaderText(headerMessage);
        alert.setContentText(infoMessage);

        Optional<ButtonType> result = alert.showAndWait();
        ButtonType button = result.orElse(ButtonType.CANCEL);

        if (button == ButtonType.OK) {
            try {
                doUpdateAttributes(getTxtName());
            } catch (Exception e) {
                UIMethods.infoBox("Not updated", "Alert");
            }

            apLibrary.getChildren().clear();
            doUpdateLibrary();
            String message = getTxtName() + " sucessfully replaced!";
            UIMethods.showUpdate(message,lblInfo);
        } else {
            System.out.println("is canceled");
        }
    }

    public void defaultDrawing() {
            
        txtSaveName.setText("");
        sliderDepth.setValue(25);
        sliderRotation.setValue(0);
        sliderSpacing.setValue(0.8);
        sliderStrokeSize.setValue(6.0);
        cmbElement.setValue("Square");
        cpFirstColorPicker.setValue(Color.valueOf("0x000000ff"));
        cpSecondColorPicker.setValue(Color.valueOf("0xffffffff"));
        setDataForGroup("XY", axisGroup);
        sliderXAnchor.setValue(getCanvasHeight() / 2);
        sliderYAnchor.setValue(getCanvasHeight() / 2);
        sliderXPosition.setValue(getCanvasHeight() / 2);
        sliderYPosition.setValue(getCanvasHeight() / 2);
        setDataForGroup("Normal", modeGroup);
        tgbDashed.setSelected(false);
        tgbQuad.setSelected(false);
        cmbBlendMode.setValue("SRC_OVER");
        setDataForGroup("Disabled", backgroundGroup);
        cmbBackImage.setValue("Circle");
        sliderInitialSize.setValue(512);
    }
    
    private double canvasSizeUpdate(){       
        return  512.00/1881.00 * splitPane.getBoundsInLocal().getMaxX();
        
    }
    
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Square.setCanvas(getCanvasHeight());
        Triangle.setCanvas(getCanvasHeight());
        Diamond.setCanvas(getCanvasHeight());
        Hexagon.setCanvas(getCanvasHeight());
        Equal.setCanvas(getCanvasHeight());
        Circle.setCanvas(getCanvasHeight());
        Squircle.setCanvas(getCanvasHeight());
        gc = canvas.getGraphicsContext2D();
        cmbElement.getItems().addAll("Square", "Circle", "Triangle", "Diamond", "Squircle", "Hexagon", "Equal");
        cmbElement.setValue("Square");

        cmbBlendMode.getItems().addAll("SRC_OVER", "SRC_ATOP", "ADD", "BLUE", "COLOR_BURN", "COLOR_DODGE", "DARKEN", "DIFFERENCE",
                "EXCLUSION", "GREEN", "HARD_LIGHT", "LIGHTEN", "MULTIPLY", "OVERLAY", "RED", "SCREEN", "SOFT_LIGHT");
        cmbBlendMode.setValue("DIFFERENCE");

        cmbBackImage.getItems().addAll("Filled", "Circle", "Heart", "Sunray", "Bladecut", "Sandglass",
                "User", "Donut", "Spark", "Top toy", "Bee", "Crossroad", "Honeycomb", "Lucky Clover");
        cmbBackImage.setValue("Filled");

        cpFirstColorPicker.setValue(Color.BLACK);

        gc.setStroke(Color.BLACK);

        rdbXY.setUserData("XY");
        rdbX.setUserData("X");
        rdbY.setUserData("Y");

        tgbNormal.setUserData("Normal");
        tgbXRay.setUserData("XRay");
        tgbDashed.setUserData("Dashed");
        tgbQuad.setUserData("Quad");

        tgbImgDisabled.setUserData("Disabled");
        tgbImgEnabled.setUserData("Enabled");

        liveAttributeChange();
        
        btnAnimate.setDisable(false);
        btnStopAnim.setDisable(true);
        //setAccelerator();
        btnStopAnim.setDisable(true); 
        
        liveDraw();
        doUpdateLibrary();

        System.out.println("Initialization done!");

    }

}
