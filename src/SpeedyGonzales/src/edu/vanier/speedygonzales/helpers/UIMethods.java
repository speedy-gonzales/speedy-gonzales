/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.helpers;

import edu.vanier.speedygonzales.controllers.CreationSceneController;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 *
 * @author Sajeevan
 */
public class UIMethods {

    public static void exportDesign(StackPane stackDesign, Canvas canvas) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter
                = new FileChooser.ExtensionFilter("png files (.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(new Stage());

        if (file != null) {
            try {
                WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
                stackDesign.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                // Logger.getLogger(JavaFX_DrawOnCanvas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

    public static Image getImg(ComboBox cmbBackImage) {
        String img = null;

        String directory = "edu/vanier/speedygonzales/images/";

        if (cmbBackImage.getValue().equals("Circle")) {
            img = directory + "circle.png";
        } else if (cmbBackImage.getValue().equals("Heart")) {
            img = directory + "heart.png";
        } else if (cmbBackImage.getValue().equals("User")) {
            img = directory + "user.png";
        } else if (cmbBackImage.getValue().equals("Sunray")) {
            img = directory + "sunray.png";
        } else if (cmbBackImage.getValue().equals("Sandglass")) {
            img = directory + "sandglass.png";
        } else if (cmbBackImage.getValue().equals("Donut")) {
            img = directory + "donut.png";
        } else if (cmbBackImage.getValue().equals("Bladecut")) {
            img = directory + "bladecut.png";
        } else if (cmbBackImage.getValue().equals("Spark")) {
            img = directory + "spark.png";
        } else if (cmbBackImage.getValue().equals("Top toy")) {
            img = directory + "spinningTop.png";
        } else if (cmbBackImage.getValue().equals("Bee")) {
            img = directory + "bee.png";
        } else if (cmbBackImage.getValue().equals("Crossroad")) {
            img = directory + "crossroad.png";
        } else if (cmbBackImage.getValue().equals("Honeycomb")) {
            img = directory + "honeycomb.png";
        } else if (cmbBackImage.getValue().equals("Lucky Clover")) {
            img = directory + "luckyclover.png";
        } else if (cmbBackImage.getValue().equals("Filled")) {
            img = directory + "filled.png";
        }

        Image image = new Image(img);

        return image;
    }

    public static byte[] imagesInBytes(StackPane stackDesign, Canvas canvas) {

        WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
        stackDesign.snapshot(null, writableImage);
        BufferedImage inputImage = SwingFXUtils.fromFXImage(writableImage, null);

        int scaleSize = 175;
        BufferedImage outputImage = new BufferedImage(scaleSize, scaleSize, inputImage.getType());

        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaleSize, scaleSize, null);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.dispose();

        byte[] imageByte = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(outputImage, "png", baos);

            baos.flush();
            imageByte = baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(CreationSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageByte;
    }
        public static void showUpdate(String message, Label lblInfo) {
        lblInfo.setStyle(
                "-fx-font-size: 16px;" +
                "-fx-text-fill: #008000;"
                + "-fx-font-family: Arial Narrow;"
 
        );
        lblInfo.setText(message);

        FadeTransition transition = new FadeTransition(Duration.seconds(8), lblInfo);
        transition.setFromValue(2);
        transition.setToValue(0);
        transition.setCycleCount(1);
        transition.play();
    }
            /**
     * method to display alert pop-up message
     *
     * @param infoMessage is the message of the alert
     * @param titleBar is the title of the alert
     */
    public static void infoBox(String infoMessage, String titleBar) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleBar);

        alert.setContentText(infoMessage);
        alert.showAndWait();
    }
    
    
}
