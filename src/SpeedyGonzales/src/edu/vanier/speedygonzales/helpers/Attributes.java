/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.helpers;

import edu.vanier.speedygonzales.elements.Element;
import javafx.scene.image.ImageView;

/**
 *
 * @author Sajeevan
 */
public class Attributes extends Element{
    public ImageView image;
    private String name;
    private String type;
    private int depth;
    private double spacingRatio; 
    private double rotationValue;
    private String axis;
    private String firstColor;
    private String secondColor;
    private double strokeSize;
    private String mode;
    private String dashed;
    private String quad;
    private String blendMode;
    private String backgroundSelection;
    private String backgroundImage;
    private double anchorX;
    private double anchorY;
    private double posX;
    private double posY;
    private double initialSize;
    
    public Attributes() {
    }

    public Attributes(String type, int depth, double spacingRatio, double rotationValue, String axis, double anchorX, double anchorY, double posX, double posY, String firstColor, String secondColor, double strokeSize, String mode, String dashed, String quad, String blendMode, String backgroundSelection, String backgroundImage, double initialSize) {
        this.type = type;
        this.depth = depth;
        this.spacingRatio = spacingRatio;
        this.rotationValue = rotationValue;
        this.axis = axis;
        this.anchorX = anchorX;
        this.anchorY = anchorY;
        this.posX = posX;
        this.posY = posY;
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.strokeSize = strokeSize;
        this.mode = mode;
        this.dashed = dashed;
        this.quad = quad;
        this.blendMode = blendMode;
        this.backgroundSelection = backgroundSelection;
        this.backgroundImage = backgroundImage;
        this.initialSize = initialSize;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }
    
    public double getAnchorX() {
        return anchorX;
    }

    public void setAnchorX(double anchorX) {
        this.anchorX = anchorX;
    }

    public double getAnchorY() {
        return anchorY;
    }

    public void setAnchorY(double anchorY) {
        this.anchorY = anchorY;
    }
    
    public String getBlendMode() {
        return blendMode;
    }

    public void setBlendMode(String blendMode) {
        this.blendMode = blendMode;
    }

    public String getBackgroundSelection() {
        return backgroundSelection;
    }

    public void setBackgroundSelection(String backgroundSelection) {
        this.backgroundSelection = backgroundSelection;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getDashed() {
        return dashed;
    }

    public void setDashed(String dashed) {
        this.dashed = dashed;
    }

    public String getQuad() {
        return quad;
    }

    public void setQuad(String quad) {
        this.quad = quad;
    }

    public Attributes(ImageView image, String name) {
        this.image = image;
        this.name = name;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public double getSpacingRatio() {
        return spacingRatio;
    }

    public void setSpacingRatio(double spacingRatio) {
        this.spacingRatio = spacingRatio;
    }

    public double getRotationValue() {
        return rotationValue;
    }

    public void setRotationValue(double rotationValue) {
        this.rotationValue = rotationValue;
    }

    public String getAxis() {
        return axis;
    }

    public void setAxis(String axis) {
        this.axis = axis;
    }

    public String getFirstColor() {
        return firstColor;
    }

    public void setFirstColor(String firstColor) {
        this.firstColor = firstColor;
    }

    public String getSecondColor() {
        return secondColor;
    }

    public void setSecondColor(String secondColor) {
        this.secondColor = secondColor;
    }

    public double getStrokeSize() {
        return strokeSize;
    }

    public void setStrokeSize(double strokeSize) {
        this.strokeSize = strokeSize;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(double initialSize) {
        this.initialSize = initialSize;
    }
    
    @Override
    public String toString() {
        return "Attributes{" + "image=" + image + ", name=" + name + ", type=" + type + ", depth=" + depth + ", spacingRatio=" + spacingRatio + ", rotationValue=" + rotationValue + ", axis=" + axis + ", firstColor=" + firstColor + ", secondColor=" + secondColor + ", strokeSize=" + strokeSize + ", mode=" + mode + ", dashed=" + dashed + ", quad=" + quad + ", blendMode=" + blendMode + ", backgroundSelection=" + backgroundSelection + ", backgroundImage=" + backgroundImage + ", anchorX=" + anchorX + ", anchorY=" + anchorY + ", posX=" + posX + ", posY=" + posY + '}';
    }



}