/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.helpers;


import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

/**
 *
 * @author Sajeevan
 */
public class UpdateDatabase {

    public static Attributes getAttribute(String name) {

        Attributes attribute = new Attributes();

        String sql = "SELECT type, depth, spacingratio, rotation, axis, anchorX, anchorY, posX, posY, firstcolor, secondcolor, strokesize, mode, dashed, quad, blendmode, backgroundselection, backgroundimage,initialsize FROM tbl_attributes WHERE name = '" + name + "'";

        try (
                Connection conn = ConnectionProvider.getInstance().getConnection("SQDatabaseAttributes.db");
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                attribute.setType(rs.getString("type"));
                attribute.setDepth(rs.getInt("depth"));
                attribute.setSpacingRatio(rs.getDouble("spacingratio"));
                attribute.setRotationValue(rs.getDouble("rotation"));
                attribute.setAxis(rs.getString("axis"));
                attribute.setAnchorX(rs.getDouble("anchorX"));
                attribute.setAnchorY(rs.getDouble("anchorY"));
                attribute.setFirstColor(rs.getString("firstcolor"));
                attribute.setSecondColor(rs.getString("secondcolor"));
                attribute.setStrokeSize(rs.getDouble("strokesize"));
                attribute.setMode(rs.getString("mode"));
                attribute.setDashed(rs.getString("dashed"));
                attribute.setQuad(rs.getString("quad"));
                attribute.setBlendMode(rs.getString("blendmode"));
                attribute.setBackgroundSelection(rs.getString("backgroundselection"));
                attribute.setBackgroundImage(rs.getString("backgroundimage"));
                attribute.setPosX(rs.getDouble("posX"));
                attribute.setPosY(rs.getDouble("posY"));
                attribute.setInitialSize(rs.getDouble("initialsize"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return attribute;
        
    }

    public static ObservableList<Attributes> getAttributeName() {

        ObservableList<Attributes> oblist = FXCollections.observableArrayList();

        String sql = "SELECT name, image FROM tbl_attributes";

        try (
                Connection conn = ConnectionProvider.getInstance().getConnection("SQDatabaseAttributes.db");
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                InputStream ip = rs.getBinaryStream("image");
                Image logo = new Image(ip);
                ImageView image = new ImageView(logo);
                image.setSmooth(true);

                oblist.add(new Attributes(image, rs.getString("name")));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return oblist;
    }

    public static void delete(String name) {

        String insertQuery = "DELETE FROM tbl_attributes WHERE name = ?";

        try (Connection dbConnection = ConnectionProvider.getInstance().getConnection("SQDatabaseAttributes.db");
                PreparedStatement pstmt = dbConnection.prepareStatement(insertQuery)) {

            // set the corresponding param
            pstmt.setString(1, name);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void saveAttributes(
            String name,
            String elemType,
            int depth,
            double ratio,
            double rotation,
            String axis,
            Color firstColor,
            Color secondColor,
            byte[] image,
            double strokeSize,
            String elemMode,
            boolean dashMode,
            boolean quadMode,
            String blendMode,
            String backgroundDisabled,
            String backgroundImg,
            double anchorX,
            double anchorY,
            double posX,
            double posY, 
            double initialSize) {

        String insertQuery = "INSERT INTO tbl_attributes (name, type, depth, spacingratio, rotation, axis, firstcolor, secondcolor, image, strokesize, mode, dashed, quad, blendmode, backgroundselection, backgroundimage, anchorX, anchorY, posX, posY, initialsize) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        ///String insertQuery = "INSERT INTO tbl_attributes (name, type) VALUES(?,?)";
        try (Connection dbConnection = ConnectionProvider.getInstance().getConnection("SQDatabaseAttributes.db");
                PreparedStatement pstmt = dbConnection.prepareStatement(insertQuery)) {

            // set parameters
            pstmt.setString(1, name);
            pstmt.setString(2, elemType);
            pstmt.setInt(3, depth);
            pstmt.setDouble(4, ratio);
            pstmt.setDouble(5, rotation);
            pstmt.setString(6, axis);
            pstmt.setString(7, firstColor.toString());
            pstmt.setString(8, secondColor.toString());
            pstmt.setBytes(9, image);
            pstmt.setDouble(10, strokeSize);
            pstmt.setString(11, elemMode);
            pstmt.setString(12, (dashMode ? "Selected" : "Not Selected"));
            pstmt.setString(13, (quadMode ? "Selected" : "Not Selected"));
            pstmt.setString(14, blendMode);
            pstmt.setString(15, backgroundDisabled);
            pstmt.setString(16, backgroundImg);
            pstmt.setDouble(17, anchorX);
            pstmt.setDouble(18, anchorY);
            pstmt.setDouble(19, posX);
            pstmt.setDouble(20, posY);
            pstmt.setDouble(21, initialSize);

            pstmt.executeUpdate();
            
            System.out.println("Stored info in database.");
        } catch (SQLException ex) {
            System.err.println("An error has occured while trying to execute query: " + insertQuery);
            System.err.println("Error message: " + ex);
        }
    }

    public static void updateAttributes(
            String name,
            String elemType,
            int depth,
            double ratio,
            double rotation,
            String axis,
            Color firstColor,
            Color secondColor,
            byte[] image,
            double strokeSize,
            String elemMode,
            boolean dashMode,
            boolean quadMode,
            String blendMode,
            String backgroundDisabled,
            String backgroundImg,
            double anchorX,
            double anchorY,
            double posX,
            double posY, 
            double initialSize) {
        String insertQuery = "UPDATE tbl_attributes SET type = ?, depth = ?, spacingratio = ?, rotation = ?, axis = ?, firstcolor = ?, secondcolor = ?, image = ?, strokesize = ?, mode = ?, dashed = ?, quad = ?, blendmode = ?, backgroundselection = ?, backgroundimage = ?, anchorX = ?, anchorY = ?, posX = ?, posY = ?, initialsize = ? WHERE name = '" + name + "'";
        ///String insertQuery = "INSERT INTO tbl_attributes (name, type) VALUES(?,?)";
        try (Connection dbConnection = ConnectionProvider.getInstance().getConnection("SQDatabaseAttributes.db");
                PreparedStatement prepStm = dbConnection.prepareStatement(insertQuery)) {

            // set parameters
            prepStm.setString(1, elemType);
            prepStm.setInt(2, depth);
            prepStm.setDouble(3, ratio);
            prepStm.setDouble(4, rotation);
            prepStm.setString(5, axis);
            prepStm.setString(6, firstColor.toString());
            prepStm.setString(7, secondColor.toString());
            prepStm.setBytes(8, image);
            prepStm.setDouble(9, strokeSize);
            prepStm.setString(10, elemMode);
            prepStm.setString(11, (dashMode ? "Selected" : "Not Selected"));
            prepStm.setString(12, (quadMode ? "Selected" : "Not Selected"));
            prepStm.setString(13, blendMode);
            prepStm.setString(14, backgroundDisabled);
            prepStm.setString(15, backgroundImg);
            prepStm.setDouble(16, anchorX);
            prepStm.setDouble(17, anchorY);
            prepStm.setDouble(18, posX);
            prepStm.setDouble(19, posY);
            prepStm.setDouble(20, initialSize);

            prepStm.executeUpdate();
            System.out.println("Updated info in database.");

        } catch (SQLException ex) {
            System.err.println("An error has occured while trying to execute query: " + insertQuery);
            System.err.println("Error message: " + ex);
        }
    }

}
