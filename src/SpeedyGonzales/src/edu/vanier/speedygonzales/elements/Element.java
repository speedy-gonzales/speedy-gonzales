  package edu.vanier.speedygonzales.elements;

import edu.vanier.speedygonzales.controllers.CreationSceneController;

/**
 * 
 * @author Speedy Gonzales
 */
public class Element extends CreationSceneController {
    
    private double width;
    private double height;
    private double coordinateX;
    private double coordinateY;
    private double anchorX;
    private double anchorY;

    public Element() {
    }
    
    public Element(double size, double achX, double achY, double posX, double posY, double canvHeight){
        width = size;
        height = size;
        
        coordinateX = (posX-canvHeight/2) - ((size - canvHeight)/2);
        coordinateY = (posY-canvHeight/2) - ((size - canvHeight)/2);
        anchorX = achX - coordinateX;
        anchorY = achY - coordinateY;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }
    
    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }
    
    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }
    
    public void setLength(double length) {
        this.width = length;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getAnchorX() {
        return anchorX;
    }

    public void setAnchorX(double anchorX) {
        this.anchorX = anchorX;
    }

    public double getAnchorY() {
        return anchorY;
    }

    public void setAnchorY(double anchorY) {
        this.anchorY = anchorY;
    }
    
    /**
     * update the stroke thickness
     * @param spacingRatio
     * @param strokeSize
     */
    public void strokeUpdator(double spacingRatio, double strokeSize) {
        strokeSize = strokeSize * spacingRatio;
    }
    
    /**
     * update x and y axis coordinates 
     * @param spacingRatio 
     * @param Anchor 
     */
    public void coordinateaUpdatorXY(double spacingRatio, double x, double y){
        anchorX = (anchorX * spacingRatio);
        anchorY = (anchorY * spacingRatio);

        coordinateX = (x - anchorX);
        coordinateY = (y - anchorY);
        
        height = height * spacingRatio;
        width = width * spacingRatio;
    }
    
    /**
     * update x axis coordinate 
     * @param spacingRatio 
     * @param Anchor 
     */
    public void coordinateaUpdatorX(double spacingRatio, double x) {
        anchorX = (anchorX * spacingRatio);
        coordinateX = (x - anchorX);
        width = width * spacingRatio;
    }

    /**
     * update y axis coordinate 
     * @param spacingRatio 
     * @param Anchor 
     */
    public void coordinateaUpdatorY(double spacingRatio, double y){
        anchorY = (anchorY * spacingRatio);
        coordinateY = (y - anchorY);
        height = height * spacingRatio;
    }
}
