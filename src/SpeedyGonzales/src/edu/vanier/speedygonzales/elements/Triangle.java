/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Tasnim
 */
public class Triangle {

    private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }
    public static void drawElement(GraphicsContext gc) {
        gc.fillPolygon(xPoints(), yPoints(), 3);
    }

    public static void drawStroke(GraphicsContext gc) {
        gc.strokePolygon(xPoints(), yPoints(), 3);
    }

    public static void drawTetra(GraphicsContext gc) {
        gc.fillPolygon(firstXPointsTetra(), firstYPointsTetra(), 3);
        gc.fillPolygon(secondXPointsTetra(), secondYPointsTetra(), 3);
        gc.fillPolygon(thirdXPointsTetra(), thirdYPointsTetra(), 3);
        gc.fillPolygon(fourthXPointsTetra(), fourthYPointsTetra(), 3);
    }

    public static void drawTetraStroke(GraphicsContext gc) {
        gc.strokePolygon(firstXPointsTetra(), firstYPointsTetra(), 3);
        gc.strokePolygon(secondXPointsTetra(), secondYPointsTetra(), 3);
        gc.strokePolygon(thirdXPointsTetra(), thirdYPointsTetra(), 3);
        gc.strokePolygon(fourthXPointsTetra(), fourthYPointsTetra(), 3);
    }
    public static double[] xPoints() {
        double xPoints[] = {0, canvasHeight / 2, canvasHeight};
        return xPoints;
    }

    public static double[] yPoints() {
        double yPoints[] = {canvasHeight, 0, canvasHeight};
        return yPoints;
    }

    public static double[] firstXPointsTetra() {
        double[] firstXPoints = {setCanvasRatio(10), setCanvasRatio(45), setCanvasRatio(80)};
        return firstXPoints;
    }

    public static double[] firstYPointsTetra() {
        double[] firstYPoints = {setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70)};
        return firstYPoints;
    }

    public static double[] secondXPointsTetra() {
        double secondXPoints[] = {canvasHeight - setCanvasRatio(10), canvasHeight - setCanvasRatio(45), canvasHeight - setCanvasRatio(80)};
        return secondXPoints;
    }

    public static double[] secondYPointsTetra() {
        double secondYPoints[] = {setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70)};
        return secondYPoints;
    }

    public static double[] thirdXPointsTetra() {
        double thirdXPoints[] = {setCanvasRatio(10), setCanvasRatio(45), setCanvasRatio(80)};
        return thirdXPoints;
    }

    public static double[] thirdYPointsTetra() {
        double thirdYPoints[] = {canvasHeight - setCanvasRatio(10), canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(10)};
        return thirdYPoints;
    }

    public static double[] fourthXPointsTetra() {
        double[] fourthXPoints = {canvasHeight - setCanvasRatio(10), canvasHeight - setCanvasRatio(45), canvasHeight - setCanvasRatio(80)};
        return fourthXPoints;
    }

    public static double[] fourthYPointsTetra() {
        double[] fourthYPoints = {canvasHeight - setCanvasRatio(10), canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(10)};
        return fourthYPoints;
    }

    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }
}
