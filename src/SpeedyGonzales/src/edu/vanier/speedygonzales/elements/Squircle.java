/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Squircle {
    
    private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }


    public static void drawElement(GraphicsContext gc){
        gc.fillRoundRect(0, 0, canvasHeight, canvasHeight, 100, 100);
    }
    
    public static void drawStroke(GraphicsContext gc) {
        gc.strokeRoundRect(0, 0, canvasHeight, canvasHeight, 100, 100);
    }
    
    public static void drawTetra(GraphicsContext gc) {
        gc.fillRoundRect(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.fillRoundRect(canvasHeight - setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.fillRoundRect(setCanvasRatio(10), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.fillRoundRect(canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
    }
    public static void drawTetraStroke(GraphicsContext gc) {
        gc.strokeRoundRect(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.strokeRoundRect(canvasHeight - setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.strokeRoundRect(setCanvasRatio(10), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
        gc.strokeRoundRect(canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(50), setCanvasRatio(50));
    }
    
    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }
}
