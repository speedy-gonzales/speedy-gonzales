/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Diamond {

    private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }
    public static void drawElement(GraphicsContext gc) {
        gc.fillPolygon(xPoints(), yPoints(), 4);
    }

    public static void drawStroke(GraphicsContext gc) {
        gc.strokePolygon(xPoints(), yPoints(), 4);
    }

    public static void drawTetra(GraphicsContext gc) {
        gc.fillPolygon(firstXPointsTetra(), firstYPointsTetra(), 4);
        gc.fillPolygon(secondXPointsTetra(), secondYPointsTetra(), 4);
        gc.fillPolygon(thirdXPointsTetra(), thirdYPointsTetra(), 4);
        gc.fillPolygon(fourthXPointsTetra(), fourthYPointsTetra(), 4);
    }

    public static void drawTetraStroke(GraphicsContext gc) {
        gc.strokePolygon(firstXPointsTetra(), firstYPointsTetra(), 4);
        gc.strokePolygon(secondXPointsTetra(), secondYPointsTetra(), 4);
        gc.strokePolygon(thirdXPointsTetra(), thirdYPointsTetra(), 4);
        gc.strokePolygon(fourthXPointsTetra(), fourthYPointsTetra(), 4);
    }
    public static double[] xPoints() {
        double xPoints[] = {0, canvasHeight / 2, canvasHeight, canvasHeight / 2};
        return xPoints;
    }

    public static double[] yPoints() {
        double yPoints[] = {canvasHeight / 2, 0, canvasHeight / 2, canvasHeight};
        return yPoints;
    }

    public static double[] firstXPointsTetra() {
        double firstXPoints[] = {0, setCanvasRatio(35), setCanvasRatio(70), setCanvasRatio(35)};
        return firstXPoints;
    }

    public static double[] firstYPointsTetra() {
        double firstYPoints[] = {setCanvasRatio(35), 0, setCanvasRatio(35), setCanvasRatio(70)};
        return firstYPoints;
    }

    public static double[] secondXPointsTetra() {
        double secondXPoints[] = {canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(35), canvasHeight, canvasHeight - setCanvasRatio(35)};
        return secondXPoints;
    }

    public static double[] secondYPointsTetra() {
        double secondYPoints[] = {setCanvasRatio(35), 0, setCanvasRatio(35), setCanvasRatio(70)};
        return secondYPoints;
    }

    public static double[] thirdXPointsTetra() {
        double thirdXPoints[] = {0, setCanvasRatio(35), setCanvasRatio(70), setCanvasRatio(35)};
        return thirdXPoints;
    }

    public static double[] thirdYPointsTetra() {
        double thirdYPoints[] = {canvasHeight - setCanvasRatio(35), canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(35), canvasHeight};
        return thirdYPoints;
    }

    public static double[] fourthXPointsTetra() {
        double fourthXPoints[] = {canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(35), canvasHeight, canvasHeight - setCanvasRatio(35)};
        return fourthXPoints;
    }

    public static double[] fourthYPointsTetra() {
        double fourthYPoints[] = {canvasHeight - setCanvasRatio(35), canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(35), canvasHeight};
        return fourthYPoints;
    }

    
    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }
}
