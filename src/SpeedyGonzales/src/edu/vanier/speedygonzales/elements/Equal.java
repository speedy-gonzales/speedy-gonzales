/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Equal {
        private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }


    public static void drawElement(GraphicsContext gc){
        gc.fillRect(setCanvasRatio(28), setCanvasRatio(24.1), setCanvasRatio(260), setCanvasRatio(40));
        gc.fillRect(setCanvasRatio(28), setCanvasRatio(251.9), setCanvasRatio(260), setCanvasRatio(40));
    }
    
    public static void drawStroke(GraphicsContext gc) {
        gc.strokeRect(setCanvasRatio(28), setCanvasRatio(24.1), setCanvasRatio(260), setCanvasRatio(40));
        gc.strokeRect(setCanvasRatio(28), setCanvasRatio(251.9), setCanvasRatio(260), setCanvasRatio(40));
    }
    
    public static void drawTetra(GraphicsContext gc) {
            gc.fillRect(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
            gc.fillRect(setCanvasRatio(10), setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));

            gc.fillRect(canvasHeight - setCanvasRatio(80), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
            gc.fillRect(canvasHeight - setCanvasRatio(80), setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));

            gc.fillRect(setCanvasRatio(10), canvasHeight - setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));
            gc.fillRect(setCanvasRatio(10), canvasHeight - setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));

            gc.fillRect(canvasHeight - setCanvasRatio(80), canvasHeight - setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));
            gc.fillRect(canvasHeight- setCanvasRatio(80), canvasHeight - setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
    }
    public static void drawTetraStroke(GraphicsContext gc) {
            gc.strokeRect(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
            gc.strokeRect(setCanvasRatio(10), setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));

            gc.strokeRect(canvasHeight - setCanvasRatio(80), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
            gc.strokeRect(canvasHeight - setCanvasRatio(80), setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));

            gc.strokeRect(10, canvasHeight - setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));
            gc.strokeRect(setCanvasRatio(10), canvasHeight - setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));

            gc.strokeRect(canvasHeight - setCanvasRatio(80), canvasHeight - setCanvasRatio(30), setCanvasRatio(70), setCanvasRatio(10));
            gc.strokeRect(canvasHeight - setCanvasRatio(80), canvasHeight - setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(10));
    }
    
    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }   
}
    
