/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Hexagon {

    private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }
    public static void drawElement(GraphicsContext gc) {
        gc.fillPolygon(xPoints(), yPoints(), 6);
    }

    public static void drawStroke(GraphicsContext gc) {
        gc.strokePolygon(xPoints(), yPoints(), 6);
    }

    public static void drawTetra(GraphicsContext gc) {
        gc.fillPolygon(firstXPointsTetra(), firstYPointsTetra(), 6);
        gc.fillPolygon(secondXPointsTetra(), secondYPointsTetra(), 6);
        gc.fillPolygon(thirdXPointsTetra(), thirdYPointsTetra(), 6);
        gc.fillPolygon(fourthXPointsTetra(), fourthYPointsTetra(), 6);
    }

    public static void drawTetraStroke(GraphicsContext gc) {
        gc.strokePolygon(firstXPointsTetra(), firstYPointsTetra(), 6);
        gc.strokePolygon(secondXPointsTetra(), secondYPointsTetra(), 6);
        gc.strokePolygon(thirdXPointsTetra(), thirdYPointsTetra(), 6);
        gc.strokePolygon(fourthXPointsTetra(), fourthYPointsTetra(), 6);
    }
    public static double[] xPoints() {
        double xPoints[] = {setCanvasRatio(21.2), setCanvasRatio(158), setCanvasRatio(294.8), setCanvasRatio(294.8), setCanvasRatio(158), setCanvasRatio(21.2)};
        return xPoints;
    }

    public static double[] yPoints() {
        double yPoints[] = {setCanvasRatio(79), 0, setCanvasRatio(79), setCanvasRatio(237), setCanvasRatio(316), setCanvasRatio(237)};
        return yPoints;
    }

    public static double[] firstXPointsTetra() {
        double firstXPoints[] = {setCanvasRatio(14.7), setCanvasRatio(45), setCanvasRatio(75.3), setCanvasRatio(75.3), setCanvasRatio(45), setCanvasRatio(14.7)};
        return firstXPoints;
    }

    public static double[] firstYPointsTetra() {
        double firstYPoints[] = {setCanvasRatio(27.5), setCanvasRatio(10), setCanvasRatio(27.5), setCanvasRatio(62.5), setCanvasRatio(80), setCanvasRatio(70.8)};
        return firstYPoints;
    }

    public static double[] secondXPointsTetra() {
        double secondXPoints[] = {setCanvasRatio(14.7) + setCanvasRatio(226), setCanvasRatio(45) + setCanvasRatio(226), setCanvasRatio(75.3) + setCanvasRatio(226), setCanvasRatio(75.3) + setCanvasRatio(226), setCanvasRatio(45) + setCanvasRatio(226), setCanvasRatio(14.7) + setCanvasRatio(226)};
        return secondXPoints;
    }

    public static double[] secondYPointsTetra() {
        double secondYPoints[] = {setCanvasRatio(27.5), setCanvasRatio(10), setCanvasRatio(27.5), setCanvasRatio(62.5), setCanvasRatio(80), setCanvasRatio(70.8)};
        return secondYPoints;
    }

    public static double[] thirdXPointsTetra() {
        double thirdXPoints[] = {setCanvasRatio(14.7), setCanvasRatio(45), setCanvasRatio(75.3), setCanvasRatio(75.3), setCanvasRatio(45), setCanvasRatio(14.7)};
        return thirdXPoints;
    }

    public static double[] thirdYPointsTetra() {
        double thirdYPoints[] = {setCanvasRatio(27.5) + setCanvasRatio(226), setCanvasRatio(10) + setCanvasRatio(226), setCanvasRatio(27.5) + setCanvasRatio(226), setCanvasRatio(62.5) + setCanvasRatio(226), setCanvasRatio(80) + setCanvasRatio(226), setCanvasRatio(70.8) + setCanvasRatio(226)};
        return thirdYPoints;
    }

    public static double[] fourthXPointsTetra() {
        double fourthXPoints[] = {setCanvasRatio(14.7) + setCanvasRatio(226), setCanvasRatio(45) + setCanvasRatio(226), setCanvasRatio(75.3) + setCanvasRatio(226), setCanvasRatio(75.3) + setCanvasRatio(226), setCanvasRatio(45) + setCanvasRatio(226), setCanvasRatio(14.7) + setCanvasRatio(226)};
        return fourthXPoints;
    }

    public static double[] fourthYPointsTetra() {
        double fourthYPoints[] = {setCanvasRatio(27.5) + setCanvasRatio(226), setCanvasRatio(10) + setCanvasRatio(226), setCanvasRatio(27.5) + setCanvasRatio(226), setCanvasRatio(62.5) + setCanvasRatio(226), setCanvasRatio(80) + setCanvasRatio(226), setCanvasRatio(70.8) + setCanvasRatio(226)};
        return fourthYPoints;
    }
    

    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }  
    
}
