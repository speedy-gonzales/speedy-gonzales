/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.speedygonzales.elements;

import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Circle {
    
    private static double canvasHeight;

    public static void setCanvas(double size) {
        canvasHeight = size;
    }


    public static void drawElement(GraphicsContext gc){
        gc.fillOval(0, 0, canvasHeight, canvasHeight);
    }
    
    public static void drawStroke(GraphicsContext gc) {
        gc.strokeOval(0, 0, canvasHeight, canvasHeight);
    }
    
    public static void drawTetra(GraphicsContext gc) {
        gc.fillOval(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70));
        gc.fillOval(canvasHeight - setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70));
        gc.fillOval(setCanvasRatio(10), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70));
        gc.fillOval(canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70));
    }
    public static void drawTetraStroke(GraphicsContext gc) {
        gc.strokeOval(setCanvasRatio(10), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70));
        gc.strokeOval(canvasHeight - setCanvasRatio(70), setCanvasRatio(10), setCanvasRatio(70), setCanvasRatio(70));
        gc.strokeOval(setCanvasRatio(10), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70));
        gc.strokeOval(canvasHeight - setCanvasRatio(70), canvasHeight - setCanvasRatio(70), setCanvasRatio(70), setCanvasRatio(70));
    }
    
    public static double setCanvasRatio(double value) {
        return value / 316 * canvasHeight;
    }
}
